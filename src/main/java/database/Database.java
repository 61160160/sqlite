/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;
// Singleton pattern

import com.mycompany.storeproject.poc.TestConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PP
 */
public class Database {

    private static Database instance = new Database();
    private Connection conn;

    private Database() {

    }

    public static Database getInstance() {

        String dbPath = "./db/store.db";
        try {
            if (instance.conn == null || instance.conn.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Database can connect");
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TestConnection.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error: JDBC is not existt");
        } catch (SQLException ex) {
            Logger.getLogger(TestConnection.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error: Database cannot connect");
        }
    
    return instance ;
}
public static void close(){
        try {
            if(instance.conn != null || !instance.conn.isClosed()){
                instance.conn.close();
                instance.conn = null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.conn = null;
    }
public  Connection getConnection(){
    return instance.conn;
}
}
